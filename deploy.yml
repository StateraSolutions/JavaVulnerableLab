---
- hosts: localhost  # Place where we are running Ansible
  connection: local # Connection 
  gather_facts: no
  vars_files:
    - cred.yml

  # Variables
  vars:
    ansible_python_interpreter: /usr/local/bin/python3
    # Default Names
    title: "Demo"
    vpc_name: "{{ title }} VPC"
    igw_name: "{{ title }} IGW"
    subnet_name: "{{ title }} Subnet"
    acl_name: "{{ title }} ACL"
    instance_name: "{{ title }} Instance" 
    security_group_name: "{{ title }} Security Group"
    route_table_name: "{{ title }} Route Table"

    # Default values for CIDR Blocks
    vpcCidrBlock: "10.0.0.0/16"
    subNetCidrBlock: "10.0.1.0/24"
    portCidrBlock: "0.0.0.0/0"
    destinationCidrBlock: "0.0.0.0/0"

    # State
    state: "present"

    # Default value of AZ & Region
    zone: "us-west-1b"
    region: "us-west-1"

  # List of tasks
  tasks:
    # # requirement

    # - name: Upgrade pip to the latest version
    #   command: python3 -m pip install --upgrade pip
    #   changed_when: false

    # - name: Install setuptools and packaging modules
    #   command: python3 -m pip install --upgrade setuptools packaging
    #   changed_when: false

    # - name: Install botocore and boto3 using pip
    #   command: python3 -m pip install botocore boto3
    #   changed_when: false

    # - name: Verify botocore installation
    #   command: python3 -c "import botocore"
    #   changed_when: false
    #   failed_when: false

    # - name: Verify boto3 installation
    #   command: python3 -c "import boto3"
    #   changed_when: false
    #   failed_when: false
    
    
    # Creating VPC
    - name: Create VPC
      ec2_vpc_net:
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        name: "{{ vpc_name }}"
        cidr_block: "{{ vpcCidrBlock }}"
        region: "{{ region }}"
        dns_support: "yes"
        dns_hostnames: "yes"
        tenancy: "default"
        state: "{{ state }}"
        resource_tags:
          Name: "{{ vpc_name }}"
      register: vpc_result
  
    # Creating Internet Gateway
    - name: Create Internet Gateway
      ec2_vpc_igw:
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        vpc_id: "{{ vpc_result.vpc.id }}" 
        region: "{{ region }}"
        state: "{{ state }}" 
        tags:
          Name: "{{ igw_name }}"
      register: igw_result

    # Creating Subnet
    - name: Create Subnet
      ec2_vpc_subnet:
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        vpc_id: "{{ vpc_result.vpc.id }}" 
        region: "{{ region }}"
        state: "{{ state }}"
        az: "{{ zone }}"  
        cidr: "{{ subNetCidrBlock }}"
        map_public: "yes"
        resource_tags:
          Name: "{{ subnet_name }}"
      register: subnet_result
  
    # Creating Security Group
    - name: Create Security Group
      ec2_group:  
        name: "{{ security_group_name }}"
        description: "{{ security_group_name }}"
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        vpc_id: "{{ vpc_result.vpc.id }}" 
        region: "{{ region }}"
        state: "{{ state }}"
        tags:
          Name: "{{ security_group_name }}"
        rules:
          - proto: tcp
            from_port: 80
            to_port: 80
            cidr_ip: "{{ portCidrBlock }}"
          - proto: tcp
            from_port: 443
            to_port: 443
            cidr_ip: "{{ portCidrBlock }}"
          - proto: tcp
            from_port: 22
            to_port: 22
            cidr_ip: "{{ portCidrBlock }}"
          - proto: icmp
            from_port: -1
            to_port: -1
            cidr_ip: 0.0.0.0/0

        rules_egress:
          - proto: tcp
            from_port: 80
            to_port: 80
            cidr_ip: "{{ portCidrBlock }}"
          - proto: tcp
            from_port: 443
            to_port: 443
            cidr_ip: "{{ portCidrBlock }}"
          - proto: tcp
            from_port: 22
            to_port: 22
            cidr_ip: "{{ portCidrBlock }}"
          - proto: icmp
            from_port: -1
            to_port: -1
            cidr_ip: 0.0.0.0/0

      register: security_group_result

    # Creating NACLs for subnet   
    - name: Create Network ACLs
      ec2_vpc_nacl:
        name: "{{ acl_name }}"
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        vpc_id: "{{ vpc_result.vpc.id }}" 
        region: "{{ region }}"
        state: "{{ state }}" 
        subnets: [ "{{ subnet_result.subnet.id }}" ]
        tags:
          Name: "{{ acl_name }}"
        ingress:
          # rule no, protocol, allow/deny, cidr, icmp_type, icmp_code, port from, port to
          - [100, 'tcp', 'allow', '0.0.0.0/0', null, null, 0, 65535]
            
        # rule no, protocol, allow/deny, cidr, icmp_type, icmp_code, port from, port to
        egress:
          - [100, 'all', 'allow', '0.0.0.0/0', null, null, 0, 65535]

    # Creating Route Table for subnet
    - name: Create Route Table
      ec2_vpc_route_table:
        aws_access_key: "{{ aws_access_key }}"
        aws_secret_key: "{{ aws_secret_key }}"
        vpc_id: "{{ vpc_result.vpc.id }}" 
        region: "{{ region }}"
        state: "{{ state }}" 
        tags:
          Name: "{{ route_table_name }}"
        subnets: [ "{{ subnet_result.subnet.id }}" ]
        routes:
          - dest: "{{ destinationCidrBlock }}" 
            gateway_id: "{{ igw_result.gateway_id }}"
      register: public_route_table

    - name: Check if EC2 instance exists
      ec2_instance_info:
        access_key: "{{ aws_access_key }}"
        secret_key: "{{ aws_secret_key }}"
        region: "{{ region }}"
        filters:
          instance-state-name: ["running","pending"]
          tag:Name: "{{ instance_name }}"
      register: ec2_instance_info 

    # Creating EC2 Instance
    - name: Create EC2 Instance
      ec2_instance:
        access_key: "{{ aws_access_key }}"
        secret_key: "{{ aws_secret_key }}"
        instance_type: t2.micro
        image_id: ami-08012c0a9ee8e21c4
        wait: true
        region: "{{ region }}"
        security_group: "{{ security_group_name }}"
        key_name: "tests"
        count: 1
        vpc_subnet_id:  "{{ subnet_result.subnet.id }}" 
        network:
          assign_public_ip: true
        tags:
          Name: "{{ instance_name }}"
      register: ec2_result
      when: ec2_instance_info.instances == []

    # Only if the creation of ec2 instance couldn't detect the public IP address or if the ec2 instance already exists
    - name: Gather information about any instance with a tag key Name
      amazon.aws.ec2_instance_info:
        access_key: "{{ aws_access_key }}"
        secret_key: "{{ aws_secret_key }}"
        region: "{{ region }}"
        filters:
          instance-state-name: ["running","pending"]
          tag:Name: "{{ instance_name }}"
      register: ec2_instance_info_gathering       


    - debug:
        var: ec2_instance_info_gathering

 
    - name: Add a new host
      add_host:
        name: "{{ item.instance_id }}"
        ansible_host: "{{ item.public_ip_address }}"
        ansible_user: ubuntu
        ansible_ssh_common_args: '-o StrictHostKeyChecking=no'
        ansible_ssh_private_key_file: "{{ item.key_name }}.pem"
        groups: "new_group"
      loop: "{{ ec2_instance_info_gathering.instances }}"
      ####wait for ec2 to be ready
    # - name: Wait for SSH to be available
    #   wait_for:
    #     host: "{{ item.public_ip_address }}"
    #     port: 22
    #     state: started
    #     delay: 100
    #     timeout: 300
    #   loop: "{{ ec2_instance_info_gathering.instances }}"

    - name: Display the new hosts
      debug:
        msg: "New host added: {{ item.instance_id }} with IP {{ item.public_ip_address }}"
      loop: "{{ ec2_instance_info_gathering.instances }}"
        


  
# DEPLOYMENT
- name: Use the new host
  hosts: new_group
  become: true
  vars:
    docker_user: "{{ lookup('env', 'DOCKER_USER') }}"
    docker_pass: "{{ lookup('env', 'DOCKER_PW') }}"
  tasks:
    - name: Install aptitude
      apt:
        name: aptitude
        state: latest
        update_cache: true
    
    - name: Copy docker-compose file to EC2
      copy: 
        src: "{{ lookup('env', 'CI_PROJECT_DIR') }}/docker-compose.yml"
        dest: "/home/ubuntu/docker-compose.yml"
        remote_src: no

    - name: Copy .env file to EC2
      copy: 
        src: "{{ lookup('env', 'CI_PROJECT_DIR') }}/.env"
        dest: "/home/ubuntu/.env"
        remote_src: no     

    - name: Install required system packages
      apt:
        pkg:
          - apt-transport-https
          - ca-certificates
          - curl
          - software-properties-common
          - python3-pip
          - virtualenv
          - python3-setuptools
        state: latest
        update_cache: true

    - name: Add Docker GPG apt Key
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Set the stable docker repository
      apt_repository: 
        repo: "deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_lsb.codename }} stable"
        state: present
        update_cache: yes   

    - name: Update apt packages cache for Docker
      apt:
        update_cache: yes
        cache_valid_time: 3600

    - name: Install Docker and related packages
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
        update_cache: true
      loop:
        - docker-ce
        - docker-ce-cli
        - containerd.io
        - docker-compose

    - name: Add Docker group
      ansible.builtin.group:
        name: docker
        state: present

    - name: Add user to Docker group
      ansible.builtin.user:
        name: "{{ ansible_user }}"
        groups: docker
        append: true


    - name: Start and enable Docker service
      service:
        name: docker
        state: started
        enabled: yes
    
    - name: Docker login
      docker_login:
        username: "{{ docker_user }}"
        password: "{{ docker_pass }}"
    

    - name: Run Docker Compose
      command: docker compose up -d
      become: yes